var path = require('path'),
    webpack = require('webpack'),
    config = require('./webpack.config');

module.exports = Object.assign(config, {
    devtool: 'eval-source-map',
    output: {
        path: path.resolve(__dirname, 'src'),
        filename: '[name].bundle.js',
        publicPath: '/'
    },

    /**
     * Dev server config.
     */
    devServer: {
        contentBase: path.resolve(__dirname, 'src/'),
        historyApiFallback: true,
        hot: true,
        inline: true,
        progress: true,
        colors: true,
        host: process.env.HOST || 'localhost',
        port: process.env.PORT || 9000
    },

    plugins: config.plugins.concat([
        new webpack.HotModuleReplacementPlugin(),
        new webpack.DefinePlugin({
            'process.env': {
                'APP_DATA': JSON.stringify({
                    environment: 'dev'
                })
            }
        })
    ])
});
