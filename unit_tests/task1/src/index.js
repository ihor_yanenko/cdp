/**
 * Created by Ihor_Yanenko on 6/13/2016.
 */

import SqlParser from './sql-parser';
import db from './db.json';

const parseDb = new SqlParser(db);
const query = [
    'SELECT * FROM employees',
    'SELECT position FROM employees',
    'SELECT name FROM employees WHERE years>5',
    'SELECT name FROM employees WHERE years>50',
    'SELECT name FROM employees WHERE years<=16',
    'SELECT name FROM employees WHERE years=16',
    'SELECT * FROM employees WHERE years=25'
];

for (let i = 0; i < query.length; i++) {
    console.log(parseDb.select(query[i]));
}

