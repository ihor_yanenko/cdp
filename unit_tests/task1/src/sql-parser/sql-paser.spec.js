/**
 * Created by Ihor_Yanenko on 6/14/2016.
 */

import Parser from './index';
import db from '../db.json';

describe('Tests suite for class Parser', () => {
    const parser = new Parser(db);
    const fakedb = {
        employees: [
            {"name": "Vasya", "years": 25, "position": "qa", "office": "1"},
            {"name": "Petya", "years": 25, "position": "front-end", "office": "1"},
            {"name": "Vasya", "years": 23, "position": "back-end", "office": "1"}
        ]
    };
    describe('select', () => {
        it('should return sample from database with all columns', () => {
            expect(parser.select("SELECT * FROM employees")).toEqual(db.employees);
        });

        it('should return sample from database with only one columns', () => {
            let sample = parser.select("SELECT name FROM employees");

            expect(sample[0].name).not.toBeUndefined();
            expect(Object.keys(sample[0]).length).toBe(1);
        });

        it('should return sample from database with one columns and with expression(WHERE "<")', () => {
            let parser = new Parser(fakedb);
            let sample = parser.select("SELECT name FROM employees WHERE years<25");

            expect(sample[0].name).not.toBeUndefined();
            expect(Object.keys(sample[0]).length).toBe(1);
            expect(sample[0].name).toEqual(fakedb.employees[2].name);
            expect(sample.length).toBe(1);
        });

        it('should return sample from database with one columns and with expression(WHERE ">")', () => {
            let parser = new Parser(fakedb);
            let sample = parser.select("SELECT name FROM employees WHERE years>23");

            expect(sample.length).toBe(2);
            expect(sample[0].name).not.toBeUndefined();
            expect(Object.keys(sample[0]).length).toBe(1);
            expect(sample[0].name).toEqual(fakedb.employees[0].name);
        });

        it('should return sample from database with all columns and with expression(WHERE "<=")', () => {
            let parser = new Parser(fakedb);
            let sample = parser.select("SELECT * FROM employees WHERE years<=25");

            expect(sample).toEqual(fakedb.employees);
        });

        it('should return sample from database with all columns and with expression(WHERE ">=")', () => {
            let parser = new Parser(fakedb);
            let sample = parser.select("SELECT * FROM employees WHERE years>=23");

            expect(sample).toEqual(fakedb.employees);
        });

        it('should return sample from database with all columns and with expression(WHERE "=")', () => {
            let parser = new Parser(fakedb);
            let sample = parser.select("SELECT * FROM employees WHERE years=25");

            expect(sample[0]).toEqual(fakedb.employees[0]);
        });
    });

    describe('_test', () => {
        it('should return true with correct query and false with incorrect', () => {
            expect(parser._test("SELECT filed FROM table123 WHERE filed123=25")).toBe(true);
            expect(parser._test("SELECT name FROM employees WHERE years>25")).toBe(true);
            expect(parser._test("SELECT name FROM employees WHERE years>=25")).toBe(true);
            expect(parser._test("SELECT some_field FROM employees WHERE years<50")).toBe(true);
            expect(parser._test("SELECT * FROM employees")).toBe(true);
            expect(parser._test("SELEC * FROM employees")).toBe(false);
        });
    });

    describe('_serialize', () => {
        it('should return serialize object of query string', () => {
            const str = "SELECT field FROM table123 WHERE field123>=25";

            expect(parser._serialize(str)).toEqual({
                select: 'field',
                from: 'table123',
                where: {
                    key: 'field123',
                    expression: '>=',
                    value: '25'
                }
            });
        });
    });

    describe('_serializeWhere', () => {
        it('should return serialize object of string', () => {
            const str = 'field123>=25';

            expect(parser._serializeWhere(str)).toEqual({
                key: 'field123',
                expression: '>=',
                value: '25'
            });
        });
    });

    describe('_find', () => {
        it('should return documents of db', () => {
            const parser = new Parser(fakedb);
            const serializeQuery = parser._serialize("SELECT * FROM employees");
            const samples = parser._find(serializeQuery);

            expect(samples).toEqual(fakedb.employees);
        });
    });

});
