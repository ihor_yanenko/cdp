class Parser {
    constructor(db) {
        this.db = db;
    }

    select(query) {
        const error = 'Query not valid';

        if (!this._test(query)) {
            return {error}
        }
        return this._find(this._serialize(query));
    }

    _test(str) {
        const reg = /SELECT\s*(.+)\s*(?:FROM)\s*(\w+)\s*((?:WHERE)\s*(\w+)(=|>|<|>=|<=)(\d))?/;
        return reg.test(str);
    }

    _serialize(query) {
        const reg = /\s+/;
        const match = query.split(reg);
        const SELECT = 'SELECT';
        const FROM = 'FROM';
        const WHERE = 'WHERE';

        var serializeQuery = {};

        match.forEach((str, i, arr) => {
            switch (str) {
                case SELECT:
                    serializeQuery.select = arr[i + 1];
                    break;
                case FROM:
                    serializeQuery.from = arr[i + 1];
                    break;
                case WHERE:
                    serializeQuery.where = this._serializeWhere(arr[i + 1]);
                    break;
                default:
                    break;
            }
        });
        return serializeQuery;
    }

    _serializeWhere(query) {
        const reg = /(\w+)(>=|<=|=|>|<)(.+)/;
        const match = query.match(reg);
        match[2] = (match[2] === '=') ? '==' : match[2];
        return {
            key: match[1],
            expression: match[2],
            value: match[3]
        }
    }

    _find(query) {
        const collection = this.db[query.from];

        return collection
            .map((doc) => {
                if (query.where) {
                    if (eval(`${doc[query.where.key]} ${query.where.expression} ${query.where.value}`)) {
                        return this._selectDocument(query, doc);
                    }
                } else {
                    return this._selectDocument(query, doc);
                }
            })
            .filter((val) => {
                return val !== undefined;
            });
    }

    _selectDocument(query, doc) {
        if (query.select !== '*') {
            let obj = {};
            obj[query.select] = doc[query.select];
            return obj;
        }
        return doc;
    }
}

export default Parser;
