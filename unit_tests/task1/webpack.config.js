var path = require('path'),
    webpack = require('webpack'),
    HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: [path.join(__dirname, 'src/index')],
    output: {},
    module: {
        preLoaders: [
            {
                test: /\.js?$/,
                loaders: ['eslint'],
                include: path.join(__dirname, 'src/app/app.js')
            }
        ],
        loaders: [
            { test: /\.js$/, exclude: [/node_modules/], loader: 'ng-annotate!babel?presets[]=es2015,presets[]=stage-2' },
            { test: /\.html$/, loader: 'raw' },
            { test: /\.css$/, loader: 'style!css' },
            { test: /\.less$/, loader: 'style!css!less' },
            { test: /\.json$/, loader: 'json' }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: 'src/index.html',
            inject: true,
            hash: true
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor',
            minChunks: function (module, count) {
                return module.resource && module.resource.indexOf(path.resolve(__dirname, 'src')) === -1;
            }
        })
    ]
};
