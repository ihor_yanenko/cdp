'use strict';
const fs = require('fs');
const filendir = require('filendir');
const path = require('path');
const UglifyJS = require('uglify-js');
const glob = require('glob');
const colors = require('colors');

const minificator = (fileName, src, dest) => {
    var newFilePath = 'all.min.js',
        fileContent = UglifyJS.minify(fileName).code;

    if (!Array.isArray(fileName)) {
        newFilePath = path.join(dest, path.relative(src, fileName));
    }

    filendir.writeFileAsync(newFilePath, fileContent, err => {
        if (err) return console.log(err);
        console.log(newFilePath.magenta)
    });
};

module.exports = (src, dest, c) => () => {
    glob(`${src}/**/*.js`, (err, files) => {
        if (!c) {
            for (var i in files) {
                minificator(files[i], src, dest);
            }
        } else {
            minificator(files, src, dest);
        }
    });
};

process.on('uncaughtException', (err) => {
    console.log(`Caught exception: ${err}`);
});
