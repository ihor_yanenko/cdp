"use strict";

module.exports.setup = (app, handlers) => {
    app.get('/', handlers.views.getIndex);
};
