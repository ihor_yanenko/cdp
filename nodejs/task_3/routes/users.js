"use strict";

module.exports.setup = (app, handlers) => {

    app.get('/api/users', handlers.users.getList);
    app.post('/api/users', handlers.users.addUser);
    app.get('/api/users/:id', handlers.users.getUser);
    app.put('/api/users/:id', handlers.users.updateUser);
    app.delete('/api/users/:id', handlers.users.removeUser);
    app.get('/api/v1/accounts', (req, res, next) => {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "X-Requested-With");
        res.status(200).json(json);
    });
};

var json = [{
    "userName": "admin1",
    "email": "admin1@test.com",
    "firstName": "John1",
    "lastName": "Doe1",
    "profile": "Undefined",
    "phoneNumber": "+0 (000) 000 00 01",
    "isActive": true
}, {
    "userName": "admin2",
    "email": "admin2@test.com",
    "firstName": "John2",
    "lastName": "Doe2",
    "profile": "Undefined",
    "phoneNumber": "+0 (000) 000 00 02",
    "isActive": true
}, {
    "userName": "adminAWS100",
    "email": "email2@mail.mu",
    "firstName": "Serge2",
    "lastName": "Dewant2",
    "profile": "Reporter",
    "phoneNumber": "-",
    "isActive": false
}];
