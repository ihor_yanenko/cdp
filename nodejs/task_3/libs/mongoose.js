"use strict";

const mongoose = require('mongoose');
const fs = require('fs');
const path = require('path');
const async = require('async');
const config = require('./config');
const winston = require('winston');

var db = mongoose.connection;

mongoose.connect(config.get('mongoose:uri'));

db.on('error', (err) => {
    winston.warn("DB error: " + err);
});

db.once('open', () => {
    winston.info("DB is open");
});

var models = {};

/**
 * Initialize all schema
 */

let init = (modelsDirectory) => {

    return new Promise((resolve, reject) => {
        fs.readdir(modelsDirectory, function (err, data) {
            if (err) {
                reject(err);
                return;
            }
            resolve(data);
        });
    })
        .then((files) => {
            files.forEach((file) => {
                let modelName = path.basename(file, '.js');

                models[modelName] = require(path.join(modelsDirectory, modelName))(mongoose);
            });
        });
};

/**
 * Return created model
 */

let model = (modelName) => {
    let name = modelName.toLowerCase();

    if (!models[name]) {
        winston.warn("model is not found");
    }

    return models[name];
};

module.exports = {
    init,
    model
};