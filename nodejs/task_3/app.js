"use strict";

const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const app = express();
const userRoutes = require('./routes/users');
const viewRouter = require('./routes/views');

let handlers = {
    views: require('./handlers/views'),
    users: require('./handlers/users')
};

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

userRoutes.setup(app, handlers);
viewRouter.setup(app, handlers);

module.exports = app;