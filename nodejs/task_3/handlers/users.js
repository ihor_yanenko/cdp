"use strict";

const modelName = 'user';


var crud = require('../libs/handlers')(modelName);

function addUser(req, res, next) {
    crud.create(req.body)
        .then((data) => {
            res.send(data);
        })
        .catch((err) => {
            if (err.name === "ValidationError") {
                res.status(500).send(err);
            } else {
                res.sendStatus(500);
            }
        });
}

function getList(req, res, next) {
    crud.list(req.query)
        .then((data) => {
            res.send({
                collection: data,
                total: data.total
            });
        })
        .catch(() => {
            res.sendStatus(500);
        });
}

function getUser(req, res, next) {
    crud.getById(req.params.id)
        .then((data) => {
            res.send(data[0]);
        })
        .catch(() => {
            res.sendStatus(500);
        });
}

function updateUser(req, res, next) {
    crud.update(req.body, req.params.id)
        .then((data) => {
            res.send(data);
        })
        .catch((err) => {
            res.status(500).send(err);
        });
}

function removeUser(req, res, next) {
    crud.remove(req.params.id)
        .then(() => {
            res.sendStatus(200);
        })
        .catch(() => {
            res.sendStatus(500);
        });
}
module.exports = {
    addUser,
    getList,
    getUser,
    updateUser,
    removeUser
};