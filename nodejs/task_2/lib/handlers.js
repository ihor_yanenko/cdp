"use strict";

const db = require('../fake-db');
module.exports = {

    usersCollection(req, res, next) {
        db.getCollection((err, data) => {

            if (err) {
                return next(err);
            }

            res.set('Content-Type', 'application/json');
            res.send(JSON.stringify(data));
        });
    },

    getUser(req, res, next) {
        db.getById(req.params.id, (err, data) => {

            if (err) {
                return next(err);
            }

            res.set('Content-Type', 'application/json');
            res.send(JSON.stringify(data));
        });
    },

    addUser(req, res, next) {
        let data = req.body;

        if (data) {
            db.create(data, (err, data) => {
                if (err) {
                    return next(err);
                }
                res.set('Content-Type', 'application/json');
                res.send(JSON.stringify(data));
            });
        }
    },

    updateUser(req, res, next) {

        if (req.body) {
            db.update(req.body, (err, data) => {

                if (err) {
                    return next(err);
                }

                res.set('Content-Type', 'application/json');
                res.send(JSON.stringify(data));
            })

        }

    },

    removeUser(req, res, next) {

        db.remove(req.params.id, (err) => {
            if (err) {
                return next(err);
            }
            res.status(200).end();
        });

    }
};