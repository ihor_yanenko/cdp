var express = require('express');
var router = express.Router();
var handlers = require('../lib/handlers');

router.get('/', handlers.usersCollection);
router.get('/:id', handlers.getUser);
router.post('/', handlers.addUser);
router.put('/:id', handlers.updateUser);
router.delete('/:id/', handlers.removeUser);

module.exports = router;
