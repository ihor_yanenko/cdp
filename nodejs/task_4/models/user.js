'use strict';

const crypto = require('crypto');
const path = require('path');

module.exports = (mongoose) => {

    let Schema = new mongoose.Schema({
        id: {
            type: mongoose.Schema.ObjectId,
            default: () => { return new mongoose.Types.ObjectId }
        },
        login: {
            type: String,
            unique: true,
            required: [true, 'Username is required']
        },
        hashedPassword: {
            type: String,
            validate: {
                validator: function () {
                    return this._plainPassword.length >= 3;
                },
                message: 'Password must be greater than or equal to 3 characters'
            },
            required: [true, 'Password is required']
        },
        role: {
            type: String,
            enum: ['user', 'admin'],
            default: 'user'
        },
        salt: String
    });

    Schema.methods.encryptPassword = function (password) {
        return crypto.createHmac('sha1', this.salt).update(password).digest('hex');
    };

    Schema.virtual('password')
        .set(function (password) {
            this._plainPassword = password;
            this.salt = Math.random() + '';
            this.hashedPassword = this.encryptPassword(password);
        })
        .get(function () {
            return this._plainPassword;
        });

    Schema.methods.verifyPassword = function (password) {
        return this.encryptPassword(password) === this.hashedPassword;
    };

    /**
     * Initialize model with the same name as a file name
     */

    return mongoose.model(path.basename(module.filename, '.js'), Schema);
};