"use strict";

const path = require('path');

module.exports = (mongoose) => {

    let Schema = new mongoose.Schema({
        id: {
            type: mongoose.Schema.ObjectId,
            default: () => { return new mongoose.Types.ObjectId }
        },
        position: String,
        company: String,
        firstName: {
            type: String,
            required: [true, 'First Name is required']
        },
        lastName: {
            type: String,
            required: [true, 'Last Name is required']
        },
        email: {
            type: String,
            validate: {
                validator: function (v) {
                    let reg = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;
                    return reg.test(v);
                },
                message: '{VALUE} is not a valid email! Please fill a valid email address'
            },
            required: [true, 'Email address is required']
        },
        phoneNumber: {
            type: String,
            validate: {
                validator: function (v) {
                    let reg = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/;
                    return reg.test(v);
                },
                message: '{VALUE} is not a valid phone number! Please fill a valid email address'
            },
            required: [true, 'Phone Number is required']
        }
    });

    /**
     * Initialize model with the same name as a file name
     */

    return mongoose.model(path.basename(module.filename, '.js'), Schema);
};
