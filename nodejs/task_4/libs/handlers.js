"use strict";

const mongoose = require('mongoose');
const db = require('./mongoose');

module.exports = function (modelName) {

    /**
     * Get all docs.
     */

    let list = (options) => {
        var query = {},
            opt = {
                skip: (options.offset - 1) * options.perPage,
                limit: parseInt(options.perPage),
                sort: {}
            };

        opt.sort[options.sortBy] = (options.sortDir === 'asc') ? -1 : 1;

        if (options.searchBy && options.searchValue) {
            query[options.searchBy] = options.searchValue;
        }

        return new Promise((resolve, reject) => {
            db.model(modelName).count({}, (err, count) => {
                db.model(modelName).find(query, '', opt, (err, data) => {
                    if (err && !data) {
                        reject(err);
                    } else {
                        data.total = count;
                        resolve(data);
                    }
                });
            });
        });
    };

    /**
     * Get doc by id.
     */

    let getById = (id) => {
       return new Promise((resolve, reject) => {
           try {
               var _id = mongoose.Types.ObjectId(id);
           }
           catch (e) {
               reject(400);
           }

           db.model(modelName).find({id: id}, (err, data) => {
               if (err || !data.length) {
                   reject(err || 404);
                   return;
               }
               if (data) {
                   resolve(data);
               }
           });
       });
    };

    /**
     * Create doc.
     */

    let create = (doc) => {
        return new Promise((resolve, reject) => {
            db.model(modelName).create(doc, (err, data) => {
                if (err) {
                    reject(err);
                    return;
                }
                resolve(data);
            });
        });

    };

    /**
     * Update doc.
     */

    let update = (doc, id) => {
       return new Promise((resolve, reject) => {
           try {
               var _id = mongoose.Types.ObjectId(id);
           }
           catch (e) {
               reject(400);
           }

           db.model(modelName).findOneAndUpdate({id: id}, {$set: doc}, {runValidators: true}, (err, doc) => {
               if (err) {
                   reject(err);
                   return;
               }

               if (doc) {
                   resolve(doc);
               } else {
                   reject(404);
               }
           });
       });
    };

    /**
     * Remove doc by id
     */

    let remove = (id) => {
       return new Promise((resolve, reject) => {
           try {
               var _id = mongoose.Types.ObjectId(id);
           }
           catch (e) {
               reject(400);
           }

           db.model(modelName).remove({id: id}, (err, data) => {
               if (err) {
                   reject(err);
               } else {
                   resolve(data);
               }
           });
       });
    };

    return {
        list,
        getById,
        create,
        update,
        remove
    }
};