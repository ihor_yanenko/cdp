"use strict";

/**
 * middleware sets roles and adds rights
 */
const pathToRegexp = require('path-to-regexp');
const config = require('./config');

const modelName = 'user';

const crud = require('../libs/handlers')(modelName);

const permissions = {
    hasAccess: function (role, url) {
        return !this[role].url.some((elem) => {
            return elem.test(url);
        });
    }
};

let handle = (req, res, next) => {
    var role = 'unregistered';

    if (req.session.user) {

        crud.getById(req.session.user.id)

            .then((user) => {
                role = user[0].role;
                if (!permissions.hasAccess(role, req.url)) {
                    res.redirect(permissions[role].redirect);
                } else {
                    req._user = user[0];
                    next();
                }
            })
            .catch((err) => {
                res.redirect(permissions[role].redirect);
            });

    } else if (!permissions.hasAccess(role, req.url)) {
        res.redirect(permissions[role].redirect);
    } else {
        next();
    }
};

let forbid = (role, rule) => {
    rule.url = rule.url.map((e) => {
        return pathToRegexp(e);
    });
    permissions[role] = rule;
};

module.exports = {
    handle,
    forbid
};