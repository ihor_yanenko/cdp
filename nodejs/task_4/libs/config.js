"use strict";

const nconf = require('nconf');

nconf
    .argv()
    .env()
    .file({ file: './config/main.json' });

module.exports = nconf;
