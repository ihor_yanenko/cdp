"use strict";

module.exports.setup = (app, handlers) => {
    app.get('/signin', handlers.login.signin);
    app.get('/register', handlers.login.register);
    app.get('/logout', handlers.login.logout);
};
