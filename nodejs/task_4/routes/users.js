"use strict";

module.exports.setup = (app, handlers) => {

    app.get('/api/users', handlers.users.getList);
    app.post('/api/users', handlers.users.addUser);
    app.get('/api/users/:id', handlers.users.getUser);
    app.put('/api/users/:id', handlers.users.updateUser);
    app.delete('/api/users/:id', handlers.users.removeUser);

};