"use strict";

module.exports.setup = (app, handlers) => {
    app.get('/', handlers.views.index);
    app.get('/home', handlers.views.home);
    app.get('/admin', handlers.views.admin);
};
