"use strict";

const modelName = 'user';
const mongoose = require('../libs/mongoose');

function index(req, res, next) {
    res.render('login_page', {title: "task4", message:''});
}
function home(req, res, next) {
    res.render('home',
        {
            title: "Hello " + req._user.login,
            user: req._user
        }
    );
}
function admin(req, res, next) {

    const Model = mongoose.model(modelName);

    Model.find({}, (err, data) => {
        if (err) {next(err);}

        res.render('admin', {users: data});
    });
}
module.exports = {
    index,
    home,
    admin
};