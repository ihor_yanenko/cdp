"use strict";

const modelName = 'user';

const mongoose = require('../libs/mongoose');

function signin(req, res, next) {

    const user = mongoose.model(modelName);

    user.find({login: req.query.login}, (err, data) => {
        if (data.length && data[0].verifyPassword(req.query.password)) {
            req.session.user = {id: data[0].id};
            res.redirect('/home');
        } else {
            res.render('login_page', {title: 'error', message: 'invalid input data', error:{status:'',stack:''}});
        }
    });
}

function register(req, res, next) {
    const Model = mongoose.model(modelName);
    var role = 'user';
    if (req.query) {
        if (req.query.admin) {
            role = 'admin';
        }

        let user = new Model({
            login: req.query.login,
            password: req.query.password,
            role: role
        });

        user.save((err) => {
            if (err) {
                res.render('error', {message: 'validation error', error: err});
            } else {
                req.session.user = {id: user.id};
                res.redirect('/home');
            }
        });
    }
}

function logout(req, res, next) {
    req.session.destroy(() => {
        res.redirect('/');
    })
}


module.exports = {
    signin,
    register,
    logout
};