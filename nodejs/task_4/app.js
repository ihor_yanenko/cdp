"use strict";

const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const app = express();

/*Configuration*/
const config = require('./libs/config');

const pass = require('./libs/pass');

pass.forbid('user', {
    url: ['/admin', '/'],
    redirect: '/home'
});
pass.forbid('unregistered', {
    url: ['/admin', '/home'],
    redirect: '/'
});
pass.forbid('admin', {
    url: ['/'],
    redirect: '/home'
});

/*Routes*/
const userRoutes = require('./routes/users');
const viewRouter = require('./routes/views');
const loginRouter = require('./routes/login');


let handlers = {
    views: require('./handlers/views'),
    users: require('./handlers/users'),
    login: require('./handlers/login')
};

app.use((req, res, next) => {
    console.log(req.url);
    next();
});


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(express.static(path.join(__dirname, 'public')));

app.use(session({
    resave: true,
    saveUninitialized: true,
    secret: config.get('session:secret'),
    store: new MongoStore({
        url: config.get('mongoose:uri')
    })
}));

/*Add access middleware*/
app.use(pass.handle);


userRoutes.setup(app, handlers);
viewRouter.setup(app, handlers);
loginRouter.setup(app, handlers);

module.exports = app;
