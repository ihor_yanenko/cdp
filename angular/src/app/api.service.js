import angular from 'angular';

class ApiService {
    constructor($http) {
        this.http = $http;
    }

    login(data) {
        return this.http({
            method: 'POST',
            url: '/login-user',
            data
        })
    }

    fetchData(method, url) {
        return this.http({
            method: method,
            url: url
        });
    }

    getCourse(id) {
        const uri = `/courses/${id}`;
        return this.fetchData('GET', uri);
    }

    addCourse(data) {
        return this.http({
            method: 'POST',
            url: '/courses/add',
            data
        });
    }
    editCourse(data) {
        const uri = `/courses/${data.id}`;
        return this.http({
            method: 'POST',
            url: uri,
            data
        });
    }
}

ApiService.$inject = ['$http'];

const ApiServiceModule = angular
    .module('cpApp.service.api', [])
    .service('ApiService', ApiService)
    .name;

export default ApiServiceModule;
