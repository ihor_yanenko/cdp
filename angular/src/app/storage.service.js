import angular from 'angular';

class StorageService {

    constructor($window) {
        this.sessionStorage = $window.sessionStorage;
    }

    setItem(key, value) {
        const serializedValue = StorageService.serialize(value);
        this.sessionStorage.setItem(key, serializedValue);
    }

    getItem(key) {
        const value = this.sessionStorage.getItem(key);
        return StorageService.unserialize(value);
    }

    removeItem(key) {
        this.sessionStorage.removeItem(key);
    }

    static serialize(value) {
        return JSON.stringify(value);
    }

    static unserialize(value) {
        return JSON.parse(value);
    }
}

StorageService.$inject = ['$window', '$rootScope'];


const storageServiceModule = angular
    .module('cpApp.service.storage', [])
    .service('StorageService', StorageService)
    .name;

export default storageServiceModule;
