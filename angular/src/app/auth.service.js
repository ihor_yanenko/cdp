import angular from 'angular';

import StorageService from './storage.service';

class AuthService {
    constructor(StorageService, $rootScope) {
        this.storageService = StorageService;
        this.rootScope = $rootScope;
    }

    isLogged() {
        return this.storageService.getItem('authData');
    }

    setSession(data) {
        Object.assign(this.rootScope.auth, data);
        this.storageService.setItem('authData', data);
    }

    removeSession() {
        this.rootScope.auth = {};
        this.storageService.removeItem('authData');
    }
}

AuthService.$inject = ['StorageService', '$rootScope'];

const authServiceModule = angular
    .module('cpApp.service.auth', [])
    .service('AuthService', AuthService)
    .name;

export default authServiceModule;