import angular from 'angular';

const sessionInjector = authService => ({
    request(config) {
        const authData = authService.isLogged();
        if (authData) {
            config.headers['x-session-token'] = authData.token;
        }
        return config;
    }
});

const sessionInjectorModule = angular
    .module('cpApp.factory.sessionInjector', [])
    .factory('sessionInjector', sessionInjector)
    .name;

sessionInjector.$inject = ['AuthService'];

export default sessionInjectorModule;
