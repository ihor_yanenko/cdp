import angular from 'angular';

import breadcrumbsComponent from './breadcrumbs.component';

const breadcrumbs = angular
    .module('cpApp.common.breadcrumbs', [])
    .component('breadcrumbs', breadcrumbsComponent)
    .name;

export default breadcrumbs;
