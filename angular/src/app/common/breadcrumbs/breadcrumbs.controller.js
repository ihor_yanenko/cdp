class BreadCrumbsController {
    constructor($rootScope, $state, $interpolate) {
        this.$rootScope = $rootScope;
        this.$state = $state;
        this.$interpolate = $interpolate;
    }

    $onInit() {
        this.breadcrumbs = this.updateBreadcrumbs();
        this.offStateChangeSuccess = this.$rootScope.$on('$stateChangeSuccess', () => {
            this.breadcrumbs = this.updateBreadcrumbs();
        });
    }

    $onDestroy() {
        this.offStateChangeSuccess();
    }

    updateBreadcrumbs() {
        const currentState = this.getCurrentState();

        let breadcrumbs = [];
        let workingState = currentState;

        while (workingState && workingState.name !== '') {
            if (workingState.breadcrumbs) {
                breadcrumbs.unshift(this.getBreadcrumb(workingState));
            }
            workingState = workingState.parent
        }
        return breadcrumbs
    }

    getBreadcrumb(state) {
        const context = (typeof state.locals !== 'undefined') ? state.locals.globals : state.breadcrumbs;
        const label = this.$interpolate(state.breadcrumbs.label)(context);
        const route = this.$interpolate(state.breadcrumbs.route)(context);

        return {
            label,
            route
        };

    }

    getCurrentState() {
        return this.$state.$current;
    }
}

BreadCrumbsController.$inject = ['$rootScope', '$state', '$interpolate'];

export default BreadCrumbsController;