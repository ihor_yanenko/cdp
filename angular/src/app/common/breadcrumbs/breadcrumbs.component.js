import controller from './breadcrumbs.controller';
import template from './breadcrumbs.template.html';
import breadcrumbs from './breadcrumbs.less';

const breadcrumbsComponent = {
    controller,
    template
};

export default breadcrumbsComponent;
