/**
 * <top-bar> component
 *
 * @author <ihor_yanenko> <ihor_yanenko@epam.com>
 */

import controller from './top-bar.controller';
import template from './top-bar.template.html';
import './top-bar.less';

const topBarComponent = {
  controller,
  template
};

export default topBarComponent;

