import angular from 'angular';

import topBarComponent from './top-bar.component';

import breadcrumbs from '../breadcrumbs';

const topBar = angular
    .module('cpApp.common.topBar', [
        breadcrumbs
    ])
    .component('topBar', topBarComponent)
    .name;

export default topBar;
