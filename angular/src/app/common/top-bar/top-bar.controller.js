/**
 * topBar component controller
 *
 * @author <ihor_yanenko> <ihor_yanenko@epam.com>
 */

class TopBarController {

    constructor($state, $rootScope, AuthService) {
        this.state = $state;
        this.rootScope = $rootScope;
        this.authService = AuthService;
    }

    logout() {
        this.authService.removeSession();
        this.rootScope.auth = {};
        this.state.go('cpApp.login');
    }
}

TopBarController.$inject = ['$state', '$rootScope', 'AuthService'];

export default TopBarController;