import angular from 'angular';
import uiRouter from 'angular-ui-router';

import screenContainerComponent from './components/screen-container';

import AuthService from './auth.service';
import StorageService from './storage.service';
import ApiService from './api.service';

import sessionInjector from './session-injector.factory';
import httpMock from './http-mock.factory';

import appComponent from './app.component';


/**
 * Bootstrap the application
 */
angular
    .module('cpApp', [
        uiRouter,
        screenContainerComponent,

        AuthService,
        StorageService,
        ApiService,
        sessionInjector,
        httpMock
    ])

    .config([
        '$stateProvider',
        '$urlRouterProvider',
        '$httpProvider',

        ($stateProvider, $urlRouterProvider, $httpProvider) => {
            $httpProvider.interceptors.push('sessionInjector');
            $urlRouterProvider.otherwise('/');
            $stateProvider
                .state('cpApp', {
                    url: '/',
                    template: '<app>Loading...</app>',
                    noLogin: true
                });
        }
    ])

    .run([
        '$rootScope',
        'AuthService',
        '$state',
        'StorageService',
        'httpMock',

        ($rootScope, AuthService, $state, storageService, httpMock) => {
            $rootScope.auth = storageService.getItem('authData') || {};
            $rootScope.$on('$stateChangeStart', (event, toState) => {
                    // Pre-routing
                    if (toState.name === 'cpApp') {
                        let state = (AuthService.isLogged()) ? 'cpApp.courses' : 'cpApp.login';

                        event.preventDefault();
                        $state.go(state);
                    }

                    if (!toState.noLogin && !AuthService.isLogged()) {
                        event.preventDefault();
                        $state.go('cpApp.login');
                    }

                    if (toState.name === 'cpApp.login' && AuthService.isLogged()) {
                        event.preventDefault();
                        $state.go($state.current.name);
                    }
                });
            httpMock.init();
        }])

    .component('app', appComponent);