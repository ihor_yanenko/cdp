import template from './screen-container.template.html';

const screenContainerComponent = {
    template
};

export default screenContainerComponent;
