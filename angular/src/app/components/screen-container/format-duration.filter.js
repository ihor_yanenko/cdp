import angular from 'angular';

const formatDuration = () => (input) => {
    const [hours, min] = [Math.floor(input / 60), Math.floor(input % 60)];

    return `${hours} h : ${min} min`;
};


const formatDurationModule = angular
    .module('cpApp.filter.formatDuration', [])
    .filter('formatDuration', formatDuration)
    .name;

export default formatDurationModule;