import template from './login-screen.template.html';
import controller from './login-screen.controller';

const loginScreenComponent = {
    template,
    controller
};

export default loginScreenComponent;
