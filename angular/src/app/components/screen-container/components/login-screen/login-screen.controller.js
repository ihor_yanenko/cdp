class LoginScreenController {
    constructor(ApiService, AuthService, $state) {
        this.api = ApiService;
        this.auth = AuthService;
        this.state = $state;
    }

    login(data) {
        this.api.login(data).then((res) => {
            const session = Object.assign({username: data.username}, res.data);

            this.auth.setSession(session);
            this.state.go('cpApp.courses');
        });
    }
}

LoginScreenController.$inject = ['ApiService', 'AuthService', '$state'];

export default LoginScreenController;
