import angular from 'angular';

import loginFormComponent from './login-form.component';

const loginForm = angular
    .module('cpApp.common.loginForm', [])
    .component('loginForm', loginFormComponent)
    .name;

export default loginForm;