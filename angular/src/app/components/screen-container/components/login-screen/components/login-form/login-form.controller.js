class LoginFormController {
    constructor() {
        this.name = '';
        this.password = '';
        this.loginPattern = /^[a-zA-Z]+$/;
        this.passwordPattern = /^[a-zA-Z0-9]+$/;
    }
    login(form) {
        if (form.$invalid) { return; }
        const data = {
            username : this.name,
            password: this.password
        };
        this.onSubmit({data});
    }
}

export default LoginFormController;
