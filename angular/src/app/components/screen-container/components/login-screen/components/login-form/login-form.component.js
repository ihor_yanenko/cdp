import controller from './login-form.controller';
import template from './login-form.template.html';
import loginForm from './login-form.less';

const loginFormComponent = {
    bindings: {
        onSubmit: '&'
    },
    controller,
    template
};

export default loginFormComponent;
