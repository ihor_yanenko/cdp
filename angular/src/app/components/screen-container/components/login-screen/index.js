import angular from 'angular';
import uiRouter from 'angular-ui-router';

import loginScreenComponent from './login-screen.component';

import loginForm from './components/login-form';

const loginScreen = angular
    .module('cpApp.screen.loginScreen', [
        uiRouter,
        loginForm
    ])
    .config(['$stateProvider', ($stateProvider) => {
        $stateProvider
            .state('cpApp.login', {
                url: 'login',
                template: '<login-screen></login-screen>',
                noLogin: true,
                breadcrumbs: {
                    label: 'login page',
                    route: 'cpApp.login'
                }
            });
    }])
    .component('loginScreen', loginScreenComponent)
    .name;

export default loginScreen;
