/**
 * manageScreen component controller
 *
 * @author <ihor_yanenko> <ihor_yanenko@epam.com>
 */

class ManageScreenController {

    constructor($state, ApiService) {
        this.state = $state;
        this.api = ApiService;
        this.course = {
            "id": "123456789",
            "name": "",
            "time": "",
            "date": "",
            "description": ""
        };
    }

    $onInit() {
        const id = this.state.params.id;

        if (id !== 'new') {
            this.api.getCourse(id).then((course) => {
                Object.assign(this.course, course.data);
            });
        }
    }

    confirm() {
        const state = this.state.params.id;

        if (state === 'new') {
            this.api.addCourse(this.course)
                .then((res) => {
                    this.state.go('cpApp.courses');
                });
        } else {
            this.api.editCourse(this.course)
                .then((res) => {
                    this.state.go('cpApp.courses');
                });
        }
    }

    cancel() {
        this.state.go('cpApp.courses');
    }
}

ManageScreenController.$inject = ['$state', 'ApiService'];

export default ManageScreenController;