/**
 *  <manageScreen> component
 *
 * @author <ihor_yanenko> <ihor_yanenko@epam.com>
 */

import angular from 'angular';
import uiRouter from 'angular-ui-router';

import manageScreenComponent from './manage-screen.component';

const manageScreen = angular
    .module('cpApp.screen.manageScreen', [
        uiRouter
    ])
    .config(['$stateProvider', ($stateProvider) => {
        $stateProvider
            .state('cpApp.courses.new-edit', {
                url: '/:id',
                template: '<manage-screen></manage-screen>',
                noLogin: false,
                breadcrumbs: {
                    label: 'course - {{id}}',
                    route: 'cpApp.courses.new-edit({id: "{{id}}" })'
                },
                resolve: {
                    id: function ($stateParams) {
                        return $stateParams.id
                    }
                }
            });
    }])
    .component('manageScreen', manageScreenComponent)
    .name;

export default manageScreen;

