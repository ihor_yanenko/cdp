/**
 * <manageScreen> component
 *
 * @author <ihor_yanenko> <ihor_yanenko@epam.com>
 */

import controller from './manage-screen.controller';
import template from './manage-screen.template.html';
import './manage-screen.less';

const manageScreenComponent = {
    controller,
    template
};

export default manageScreenComponent;