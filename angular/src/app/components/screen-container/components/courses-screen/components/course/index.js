import angular from 'angular';

import courseComponent from './course.component';

const course = angular
    .module('cpApp.screen.course', [])
    .component('course', courseComponent)
    .name;

export default course;
