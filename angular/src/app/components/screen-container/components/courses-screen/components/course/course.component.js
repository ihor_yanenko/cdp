import template from './course.template.html';
import course from './course.less';

const courseComponent = {
    bindings: {
        data: '<',
        edit: '&',
        remove: '&'
    },
    template
};

export default courseComponent;
