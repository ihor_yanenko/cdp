import template from './controls.template.html';
import constrols from './controls.less';

const coursesScreenComponent = {
    bindings: {
        onSearch: '&'
    },
    template
};

export default coursesScreenComponent;
