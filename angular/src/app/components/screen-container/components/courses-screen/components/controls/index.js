import angular from 'angular';

import controlsComponent from './controls.component';

const controls = angular
    .module('cpApp.screen.controls', [])
    .component('controls', controlsComponent)
    .name;

export default controls;
