import angular  from 'angular';
import uiRouter from 'angular-ui-router';

import coursesScreenComponent from './courses-screen.component';

import controls from './components/controls';
import course   from './components/course';

const coursesScreen = angular
    .module('cpApp.screen.coursesScreen', [
        uiRouter,
        controls,
        course
    ])
    .config(['$stateProvider', ($stateProvider) => {
        $stateProvider
            .state('cpApp.courses', {
                url: 'courses',
                template: '<courses-screen></courses-screen>',
                noLogin: false,
                breadcrumbs: {
                    label: 'courses',
                    route: 'cpApp.courses'
                }
            });
    }])
    .component('coursesScreen', coursesScreenComponent)
    .name;

export default coursesScreen;
