import template from './courses-screen.template.html';
import coursesScreen from './courses-screen.less';
import controller from './courses-screen.controller';

const coursesScreenComponent = {
    template,
    controller: controller
};

export default coursesScreenComponent;
