class CoursesScreenController {
    constructor(ApiService, $rootScope) {
        this.api = ApiService;
        this.$rootScope = $rootScope;
        this.query = '';
    }

    $onInit() {
        this.fetchCourses();
        this.offStateChangeSuccess = this.$rootScope.$on('$stateChangeSuccess', (event, toState, toParams, fromState) => {
            if (fromState.name === 'cpApp.courses.new-edit') {
                this.fetchCourses();
            }
        });
    }

    $onDestroy() {
        this.offStateChangeSuccess();
    }

    fetchCourses() {
        this.api.fetchData('GET', '/courses/all')
            .then((response)=> {
                this.courses = response.data.courses;
            });
    }

    removeCourse(id) {
        const uri = `/courses/${id}`;

        this.api.fetchData('DELETE', uri)
            .then((response) => {
                const index = this.courses.findIndex(i => i.id === id);
                this.courses.splice(index, 1);
            });
    }

    search(params) {
        this.query = params.query;
    }

}

CoursesScreenController.$inject = ['ApiService', '$rootScope'];

export default CoursesScreenController;