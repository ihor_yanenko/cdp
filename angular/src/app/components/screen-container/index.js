import angular from 'angular';
import uiRouter from 'angular-ui-router';

/**
 * Dependencies: components
 */
import topBar from '../../common/top-bar';
import loginScreen from './components/login-screen';
import coursesScreen from './components/courses-screen';
import manageScreen from './components/manage-screen';

import formatDuration from './format-duration.filter';

import screenContainerComponent from './screen-container.component';

const screenContainer = angular
    .module('cpApp.component.screenContainer', [
        uiRouter,
        topBar,
        loginScreen,
        coursesScreen,
        manageScreen,

        formatDuration
    ])
    .component('screenContainer', screenContainerComponent)
    .name;

export default screenContainer;
