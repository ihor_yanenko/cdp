import angular from 'angular';
import mocks from 'angular-mocks';

import courses from './api/courses.json';

const db = JSON.parse(courses);

const httpMock = $httpBackend => ({
    init() {
        $httpBackend.whenRoute('POST', '/login-user')
            .respond((method, url, data, headers, params) => {
                return [200, {username: JSON.parse(data).username, token: 'qwerty123'}];
            });
        $httpBackend.whenRoute('GET', '/courses/:id')
            .respond((method, url, data, headers, params) => {
                return (params.id === 'all') ? [200, db] : [200, db.courses[parseInt(params.id - 1)]];
            });
        $httpBackend.whenRoute('POST', '/courses/:id')
            .respond((method, url, data, headers, params) => {
                const parseData = JSON.parse(data);

                if (params.id === 'add') {
                    db.courses.push(parseData);
                } else {
                    db.courses[parseInt(params.id) - 1] = parseData;
                }
                return [200];
            });
        $httpBackend.whenRoute('DELETE', '/courses/:id')
            .respond((method, url, data, headers, params) => {
                return [200];
            });
    }
});

const httpMockModule = angular
    .module('cpApp.factory.httpMock', [
        'ngMockE2E'
    ])
    .factory('httpMock', httpMock)
    .name;

httpMock.$inject = ['$httpBackend'];

export default httpMockModule;

