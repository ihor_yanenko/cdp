import template from './app.template.html';
import './app.less';

const appComponent = {
    template
};

export default appComponent;
