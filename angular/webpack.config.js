'use strict';

const webpack = require('webpack');
const NODE_ENV = process.env.NODE_ENV || 'DEV';
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: path.join(__dirname, 'src/app'),
    output: {
        path: path.resolve(__dirname, 'src'),
        filename: '[name].bundle.js',
        publicPath: '/'
    },

    module: {
        loaders: [
            {
                test    : /\.js$/,
                exclude : /node_modules/,
                loader  : 'babel',
                query   :
                    {
                        presets  : ['es2015', 'stage-2'],
                        plugins  : ['transform-runtime']
                    }
            },
            { test : /\.html$/, loader : 'raw' },
            { test : /\.css$/,  loader : 'style!css' },
            { test : /\.less$/, loader : 'style!css!less' },
            { test : /\.json$/, loader : 'raw' }
        ]
    },

    resolve: {
        modulesDirectories: ['node_modules'],
        extensions: ['', '.js']
    },

    resolveLoader: {
        modulesDirectories: ['node_modules'],
        modulesTemplates: ['*-loader', '*'],
        extensions: ['', '.js']
    },

    watch: NODE_ENV === 'DEV',
    watchOptions: {
        aggregateTimeout: 100
    },

    devtool: NODE_ENV === 'DEV' ? "cheap-module-source-map" : null,

    /**
     * Dev server config.
     */
    devServer: {
        contentBase: path.resolve(__dirname, 'src/'),
        historyApiFallback: true,
        hot: true,
        inline: true,
        progress: true,
        colors: true,
        host: process.env.HOST || 'localhost',
        port: process.env.PORT || 9000
    },

    plugins: [
        new webpack.DefinePlugin({
            NODE_ENV: JSON.stringify(NODE_ENV)
        }),
        new HtmlWebpackPlugin({
            template: 'src/index.html',
            inject: true,
            hash: true
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor',
            minChunks: function (module, count) {
                return module.resource && module.resource.indexOf(path.resolve(__dirname, 'src')) === -1;
            }
        }),
        new webpack.HotModuleReplacementPlugin()
    ]
};
